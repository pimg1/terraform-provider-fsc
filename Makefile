# =============================================================================
#
default: build

# Compile TF provider
.PHONY: build
build:
	go build

# Install TF provider to local registry
.PHONY: install
install: build
	mkdir -p ~/.terraform.d/plugins/terraform.local/local/fsc/0.1.0/${PLATFORM}/
	cp terraform-provider-fsc ~/.terraform.d/plugins/terraform.local/local/fsc/0.1.0/${PLATFORM}/terraform-provider-fsc_v0.1.0

# Regenerate provider docs
.PHONY: docs
docs: docs
	tfplugindocs

# Run acceptance tests
.PHONY: test
test:
	TF_ACC=1 go test ./... -v -timeout 30s -count=1

# =============================================================================

KERNEL := $(shell uname -s | tr A-Z a-z)
MACHINE := $(shell uname -m)
ifeq ($(MACHINE),i686)
  ISA += 386
else ifeq ($(MACHINE),x86_64)
  ISA += amd64
else ifeq ($(MACHINE),arm)
  ISA += arm
else ifeq ($(MACHINE),arm64)
  ISA += arm64
else ifeq ($(MACHINE),aarch64)
  ISA += arm64
endif
PLATFORM := ${KERNEL}_${ISA}

# =============================================================================
