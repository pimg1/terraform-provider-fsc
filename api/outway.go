package fsc

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func (c *Client) GetOutways() ([]Outway, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/outways", c.ControllerEndpoint), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doControllerRequest(req)
	if err != nil {
		return nil, err
	}

	response := GetOutwaysResponse{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return response.Outways, nil
}

func (c *Client) GetOutway(outwayId string) (*Outway, error) {
	outways, err := c.GetOutways()
	if err != nil {
		return nil, err
	}

	for _, outway := range outways {
		if outway.Name == outwayId {
			return &outway, nil
		}
	}

	return nil, nil
}
