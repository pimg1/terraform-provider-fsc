package fsc

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func (c *Client) GetInways() ([]Inway, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/inways", c.ControllerEndpoint), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doControllerRequest(req)
	if err != nil {
		return nil, err
	}

	response := GetInwaysResponse{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	inways := []Inway{}
	for _, inway := range response.Inways {
		inways = append(inways, Inway{
			Name:    inway.Name,
			Address: inway.Address,
		})
	}

	return inways, nil
}

func (c *Client) GetInway(inwayId string) (*Inway, error) {
	inways, err := c.GetInways()
	if err != nil {
		return nil, err
	}

	for _, inway := range inways {
		if inway.Name == inwayId {
			return &inway, nil
		}
	}

	return nil, nil
}
