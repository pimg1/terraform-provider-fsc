// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"context"
	"fmt"

	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/datasource/schema"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	fsc "terraform-provider-fsc/api"
)

var _ datasource.DataSource = &OutwayDataSource{}

func NewOutwayDataSource() datasource.DataSource {
	return &OutwayDataSource{}
}

type OutwayDataSource struct {
	client *fsc.Client
}

type OutwayDataSourceModel struct {
	Name                  types.String `tfsdk:"name"`
	CertificateThumbprint types.String `tfsdk:"certificate_thumbprint"`
	PeerId                types.String `tfsdk:"peer_id"`
}

func (d *OutwayDataSource) Metadata(ctx context.Context, req datasource.MetadataRequest, resp *datasource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_outway"
}

func (d *OutwayDataSource) Schema(ctx context.Context, req datasource.SchemaRequest, resp *datasource.SchemaResponse) {
	resp.Schema = schema.Schema{
		// This description is used by the documentation generator and the language server.
		MarkdownDescription: "Fetches a FSC Outway",

		Attributes: map[string]schema.Attribute{
			"name": schema.StringAttribute{
				MarkdownDescription: "Name of FSC outway to fetch",
				Required:            true,
			},
			"certificate_thumbprint": schema.StringAttribute{
				MarkdownDescription: "Name of FSC outway to fetch",
				Computed:            true,
			},
			"peer_id": schema.StringAttribute{
				MarkdownDescription: "Name of FSC outway to fetch",
				Computed:            true,
			},
		},
	}
}

func (d *OutwayDataSource) Configure(ctx context.Context, req datasource.ConfigureRequest, resp *datasource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	client, ok := req.ProviderData.(*fsc.Client)

	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Data Source Configure Type",
			fmt.Sprintf("Expected *fsc.Client, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	d.client = client
}

func (d *OutwayDataSource) Read(ctx context.Context, req datasource.ReadRequest, resp *datasource.ReadResponse) {
	var plan, state OutwayDataSourceModel

	// Read Terraform configuration data into the model
	resp.Diagnostics.Append(req.Config.Get(ctx, &plan)...)
	resp.Diagnostics.Append(resp.State.Get(ctx, &state)...)

	if resp.Diagnostics.HasError() {
		return
	}

	outway, err := d.client.GetOutway(plan.Name.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error retrieving Outway",
			"Could not enumerate outways: "+err.Error(),
		)
		return
	}

	if outway == nil {
		resp.Diagnostics.AddError(
			"Error retrieving Outway",
			"Could not find a matching Outway",
		)
		return
	}

	peer, err := d.client.GetPeerInfo()
	if err != nil {
		resp.Diagnostics.AddError(
			"Error retrieving Outway",
			"Could not determine Outway Peer ID: "+err.Error(),
		)
		return
	}

	state.Name = types.StringValue(outway.Name)
	state.CertificateThumbprint = types.StringValue(outway.CertificateThumbprint)
	state.PeerId = types.StringValue(peer.Id)

	tflog.Trace(ctx, "read an outway data source")

	// Save data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &state)...)
}
