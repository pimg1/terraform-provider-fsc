// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"os"
	"testing"

	"github.com/hashicorp/terraform-plugin-framework/providerserver"
	"github.com/hashicorp/terraform-plugin-go/tfprotov6"
	fsc "terraform-provider-fsc/api"
)

// testAccProtoV6ProviderFactories are used to instantiate a provider during
// acceptance testing. The factory function will be invoked for every Terraform
// CLI command executed to create a provider server to which the CLI can
// reattach.
var testAccProtoV6ProviderFactories = map[string]func() (tfprotov6.ProviderServer, error){
	"fsc": providerserver.NewProtocol6WithError(New("test")()),
}

var (
	groupId                 = os.Getenv("FSC_GROUP_ID")
	controllerEndpoint      = os.Getenv("FSC_CONTROLLER_ENDPOINT")
	managerEndpoint         = os.Getenv("FSC_MANAGER_ENDPOINT")
	CaCertsPem              = os.Getenv("FSC_CA_CERTS")
	controllerClientCertPem = os.Getenv("FSC_CONTROLLER_CLIENT_CERT")
	controllerClientKeyPem  = os.Getenv("FSC_CONTROLLER_CLIENT_KEY")
	managerClientCertPem    = os.Getenv("FSC_MANAGER_CLIENT_CERT")
	managerClientKeyPem     = os.Getenv("FSC_MANAGER_CLIENT_KEY")

	controllerCerts = []string{CaCertsPem, controllerClientCertPem, controllerClientKeyPem}
	managerCerts    = []string{CaCertsPem, managerClientCertPem, managerClientKeyPem}

	apiClient, apiClientErr = fsc.NewClient(groupId, controllerEndpoint, managerEndpoint, controllerCerts, managerCerts)
)

func testAccPreCheck(t *testing.T) {
	// You can add code here to run prior to any test case execution, for example assertions
	// about the appropriate environment variables being set are common to see in a pre-check
	// function.
}

func testAccFscClientCheck(t *testing.T) {
	if apiClientErr != nil {
		t.Logf("Failed to create FSC API client: %s", apiClientErr)
		t.FailNow()
	}
	return
}
