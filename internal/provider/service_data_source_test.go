// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"fmt"
	"testing"

	"github.com/hashicorp/terraform-plugin-testing/helper/resource"
)

func TestAccServiceDataSource(t *testing.T) {
	serviceName := "service_name"
	endpointUrl := "https://my_api:443"
	inwayAddress := "https://inway:443"

	resource.Test(t, resource.TestCase{
		PreCheck:                 func() { testAccPreCheck(t) },
		ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
		Steps: []resource.TestStep{
			// Read testing
			{
				Config: testAccServicesDataResourceConfig(serviceName, endpointUrl, inwayAddress),
				Check: resource.ComposeAggregateTestCheckFunc(
					resource.TestCheckResourceAttr("data.fsc_service.bar", "name", serviceName),
					resource.TestCheckResourceAttr("data.fsc_service.bar", "endpoint_url", endpointUrl),
					resource.TestCheckResourceAttr("data.fsc_service.bar", "inway_address", inwayAddress),
				),
			},
		},
	})
}

func testAccServicesDataResourceConfig(name, endpointUrl, inwayAddress string) string {
	return fmt.Sprintf(`
resource "fsc_service" "foo" {
  name          = %[1]q
  endpoint_url  = %[2]q
  inway_address = %[3]q
}

data "fsc_service" "bar" {
  name          = %[1]q

  depends_on = [
    fsc_service.foo
  ]
}
`, name, endpointUrl, inwayAddress)
}
