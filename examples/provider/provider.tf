provider "fsc" {
  group_id               = "fsc-demo"
  controller_endpoint    = "https://controller:444/"
  manager_endpoint       = "https://manager:443/"
  ca_certs               = "-----BEGIN CERTIFICATE...END CERTIFICATE-----"
  controller_client_cert = "-----BEGIN CERTIFICATE...END CERTIFICATE-----"
  controller_client_key  = "-----BEGIN PRIVATE KEY...END PRIVATE KEY-----"
  manager_client_cert    = "-----BEGIN CERTIFICATE...END CERTIFICATE-----"
  manager_client_key     = "-----BEGIN PRIVATE KEY...END PRIVATE KEY-----"
}
